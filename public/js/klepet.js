var Klepet = function(socket) {
  this.socket = socket;
};

Klepet.prototype.posljiSporocilo = function(kanal, besedilo) {
  var sporocilo = {
    kanal: kanal,
    besedilo: besedilo
  };
  this.socket.emit('sporocilo', sporocilo);
};

Klepet.prototype.spremeniKanal = function(kanal) {
  this.socket.emit('pridruzitevZahteva', {
    novKanal: kanal
  });
};

Klepet.prototype.procesirajZasebno = function(besede) {
  var sporocilo = false;
  if (besede.length > 1) {
    if (besede[0].charAt(0) == '\"' && besede[0].charAt(besede[0].length-1) == '\"') {
      var komu = besede[0].substring(1, besede[0].length-1);
      besede.shift();
      var zasebnoSporocilo = besede.join(" ");
      if (zasebnoSporocilo.charAt(0) == '\"' && zasebnoSporocilo.charAt(zasebnoSporocilo.length-1) == '\"') {
        zasebnoSporocilo = zasebnoSporocilo.substring(1, zasebnoSporocilo.length-1);
        this.socket.emit('zasebnoSporociloZahteva', {
          prejemnik: komu, 
          sporocilo: zasebnoSporocilo
        });
        sporocilo = "(zasebno sporocilo za " + komu + "): " + zasebnoSporocilo;
        return sporocilo;
      }
      else {
        sporocilo = 'Neznan ukaz.';
        return sporocilo;  
      }
    }
    else {
      sporocilo = 'Neznan ukaz.';
      return sporocilo;
    }
  }
  else {
    sporocilo = 'Neznan ukaz.';
    return sporocilo;
  }
};

Klepet.prototype.procesirajUkaz = function(ukaz) {
  var besede = ukaz.split(' ');
  ukaz = besede[0].substring(1, besede[0].length).toLowerCase();
  var sporocilo = false;

  switch(ukaz) {
    case 'pridruzitev':
      besede.shift();
      var kanal = besede.join(' ');
      this.spremeniKanal(kanal);
      break;
    case 'vzdevek':
      besede.shift();
      var vzdevek = besede.join(' ');
      this.socket.emit('vzdevekSpremembaZahteva', vzdevek);
      break;
    case 'zasebno':
      besede.shift();
      sporocilo = this.procesirajZasebno(besede);
      break;
    default:
      sporocilo = 'Neznan ukaz.';
      break;
  };

  return sporocilo;
};