function vstaviSmejkote(zaSpremembo) {
  zaSpremembo.html(zaSpremembo.html().replace(/:\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">'));
  zaSpremembo.html(zaSpremembo.html().replace(/;\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png">'));
  zaSpremembo.html(zaSpremembo.html().replace(/:\*/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png">'));
  zaSpremembo.html(zaSpremembo.html().replace(/:\(/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png">'));
  zaSpremembo.html(zaSpremembo.html().replace(/\(y\)/g, '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png">'));
  return zaSpremembo;
}

function divElementEnostavniTekst(sporocilo) {
  return $('<div style="font-weight: bold"></div>').text(sporocilo);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function zamenjajKletvice(text) {
  for(var i = 0; i < besede.length; i++) {
    var nadomestni = "";
    for(var j = 0; j < besede[i].length; j++) {
      nadomestni = nadomestni + "*";
    }
    text = text.replace(new RegExp( '\\b' + besede[i] + '\\b', 'gi'), nadomestni);
  }
  return text;
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {

    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    
    //$('#sporocila').append(divElementEnostavniTekst(sporocilo));
    sporocilo = zamenjajKletvice(sporocilo);
    var zaSpremembo = divElementEnostavniTekst(sporocilo);
    zaSpremembo = vstaviSmejkote(zaSpremembo);
    
    $('#sporocila').append(zaSpremembo);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();
var trenutniVzdevek;
var trenutniKanal1;
var besede;

function spremeni() {
  $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal1);
}

$(document).ready(function() {
  var klepetApp = new Klepet(socket);

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      trenutniVzdevek = rezultat.vzdevek;
      spremeni();
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal1 = rezultat.kanal;
    //$('#kanal').text(rezultat.kanal);
    spremeni();
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var text = sporocilo.besedilo;
    text = zamenjajKletvice(text);
    var novElement = $('<div style="font-weight: bold"></div>').text(text);
    novElement = vstaviSmejkote(novElement);
    $('#sporocila').append(novElement);
  });
  
  socket.on('seznamUporabnikov', function (sporocilo) {
    $('#seznam-uporabnikov').html(sporocilo.besedilo);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

$.get("https://ois-dn2-c9-tinak42.c9.io/swearWords.txt", function(data){
  besede = data.split("\r\n");
  //var i;
  //for(i = 0;i<besede.length;i++)
    //$('#sporocila').append(divElementEnostavniTekst(besede[i]));
});